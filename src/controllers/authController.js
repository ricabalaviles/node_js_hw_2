const express = require('express');
const router = express.Router();
// const {User} = require('../models/userModel') //перенесено в сервисы

const {
  registration,
  logIn,
} = require('../services/authService');// подключаем сервис

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;
  console.log(req.body);
  await registration({username, password});
  res.json({message: 'Success'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const token = await logIn({username, password});
  res.json({'message': 'Success', 'jwt_token': token});
}));

module.exports = {
  authRouter: router,
};
