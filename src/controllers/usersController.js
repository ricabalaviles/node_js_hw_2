const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const {
  getUsersProfileInfo,
  deleteUsersProfile,
  changeUsersPassword,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUsersProfileInfo(userId);
  res.json({
    'user': {
      '_id': userId,
      'username': user.username,
      'createdDate': user.createdDate,
    },
  });
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUsersProfile(userId);
  res.json({'message': 'Success'});
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const {userId} = req.user;
  const user = await User.findOne({_id: userId});
  console.log(user);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid  password');
  } else {
    await changeUsersPassword(userId, newPassword);
    res.json({'message': 'Success'});
  }
}));


module.exports = {
  usersRouter: router,
};
