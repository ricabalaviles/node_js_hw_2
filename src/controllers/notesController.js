const express = require('express');
const router = express.Router();
const {Note} = require('../models/noteModel');

const {
  getUsersNotes,
  addNoteForUser,
  getUsersNoteById,
  updateUsersNoteById,
  checkingUsersNoteById,
  deleteUsersNoteById,
} = require('../services/noteServise');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const offset = parseInt(req.query.offset, 10) || 0;
  const limit = parseInt(req.query.limit, 10) || 5;

  const notes = await getUsersNotes(userId, offset, limit);
  const count = await Note.count({userId});

  res.json({
    'offset': offset,
    'limit': limit,
    'count': count,
    notes,
  });
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await addNoteForUser(userId, req.body);
  res.json({message: 'Success'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const noteId = req.params.id;
  const {userId} = req.user;
  const note = await getUsersNoteById(noteId, userId);
  res.json({'note': {
    '_id': note._id,
    'userId': note.userId,
    'completed': note.completed,
    'text': note.text,
    'createdDate': note.createdDate,
  }});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const noteId = req.params.id;
  const data = req.body;
  const {userId} = req.user;
  await updateUsersNoteById(noteId, userId, data);

  res.json({'message': 'Success'});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
  const noteId = req.params.id;
  const {userId} = req.user;
  await checkingUsersNoteById(noteId, userId);

  res.json({'message': 'Success'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const noteId = req.params.id;
  const {userId} = req.user;
  await deleteUsersNoteById(noteId, userId);
  res.json({'message': 'Success'});
}));

module.exports = {
  notesRouter: router,
};
