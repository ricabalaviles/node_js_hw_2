const asyncWrapper = (cb) => {
  return (req, res, next) =>
    cb(req, res)
        .catch(next);
};
module.exports = {
  asyncWrapper,
};
