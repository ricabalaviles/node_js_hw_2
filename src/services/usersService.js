const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUsersProfileInfo = async (userId) => {
  const user = await User.findById(userId);
  return user;
};

const deleteUsersProfile = async (userId) => {
  await User.findByIdAndRemove(userId);
};

const changeUsersPassword = async (userId, newPassword) => {
  const changedPassword = await bcrypt.hash(newPassword, 10);
  await User.findOneAndUpdate({_id: userId},
      {$set: {password: changedPassword}});
};

module.exports = {
  getUsersProfileInfo,
  deleteUsersProfile,
  changeUsersPassword,
};
