
const {Note} = require('../models/noteModel');

const getUsersNotes = async (userId, offset, limit) => {
  const notes = await Note.find({userId: userId}, '-__v')
      .skip(offset)
      .limit(limit);
  return notes;
};

const addNoteForUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
};

const getUsersNoteById = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId}, '-__v');
  if (!note) {
    throw new Error('Nothing fount with this id');
  }
  return note;
};

const updateUsersNoteById = async (noteId, userId, data) => {
  const note = await Note.findOneAndUpdate({_id: noteId, userId},
      {$set: data});
  if (!note) {
    throw new Error('Nothing fount with this id');
  }
};

const checkingUsersNoteById = async (noteId, userId) => {
  const note = await Note.findOneAndUpdate({_id: noteId, userId},
      //   {'$set': {'completed': true}});
      [{$set: {completed: {$eq: [false, '$completed']}}}]);
    //   {$set: {completed: {$not: '$completed'}}});

  if (!note) {
    throw new Error('Nothing fount with this id');
  }

//   if (!note.completed) {
//     note.completed = true;
//   } else {
//     note.completed = false;
//   }
//   note.save();
  // return note
};

const deleteUsersNoteById = async (noteId, userId) => {
  const note = await Note.findOneAndDelete({_id: noteId, userId});
  if (!note) {
    throw new Error('Nothing fount with this id');
  }
};

module.exports={
  getUsersNotes,
  addNoteForUser,
  getUsersNoteById,
  updateUsersNoteById,
  checkingUsersNoteById,
  deleteUsersNoteById,
};
