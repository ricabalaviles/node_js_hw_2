const express = require('express');
// const path = require('path')
const morgan = require('morgan');
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const app = express();
require('dotenv').config();
const mongoURL = 'mongodb+srv://new-user-1983:pgjbcpgjbc@cluster0.7tfau.mongodb.net/hw2?retryWrites=true&w=majority'
app.set('port', process.env.PORT || 8080);

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {usersRouter} = require('./controllers/usersController');

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authRouter);

app.use(authMiddleware);

app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

app.use((req, res, nest) => {
  res.status(404);
});

app.use((err, req, res, nest) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    app.listen(app.get('port'));
  } catch (err) {
    console.log(`Error on server startup: ${err.message}`);
  }
};

start();

